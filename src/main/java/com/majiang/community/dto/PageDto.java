package com.majiang.community.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yu
 * @date 2021/8/16 14:44
 */
@Data
public class PageDto {

    private List<QuestionDTO> questionDTOList;

    /**
     * 时候存在向前
     */
    private Boolean showPrevious;

    private Boolean showFirstPage;

    private Boolean showNext;

    private Boolean showEndPage;

    private Integer page;

    private List<Integer> pages = new ArrayList<>();

    private Integer totalPage;

    public void setPageDto(Integer count, Integer page, Integer rows) {

        totalPage = 0;
        if (count % rows == 0) {
            totalPage = count / rows;
        }else{
            totalPage = count / rows + 1;
        }
        if (page < 1) {
            page = 1;
        }
        if(page > totalPage){
            page = totalPage;
        }
        this.page = page;
        pages.add(page);
        for (int i = 1; i <= 3; i++) {
            if(page - i > 0){
                pages.add(0, page - i);
            }
            if(page + i <= totalPage){
                pages.add(page + i);
            }
        }

//        是否展示上一页
        showPrevious = page == 1 ? false : true;
//        是否展示下一页
        showNext = page.equals(totalPage) ? false : true;
//        是否展示第一页
        showFirstPage = pages.contains(1) ? false : true;
//        是否展示第一页
        showEndPage = pages.contains(totalPage) ? false : true;
    }
}
