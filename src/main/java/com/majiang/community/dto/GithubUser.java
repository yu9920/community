package com.majiang.community.dto;

import lombok.Data;

/**
 * @author yu
 * @date 2021/8/12 17:43
 */
@Data
public class GithubUser {

    private String name;

    private Long id;

    private String bio;

    private String avatar_url;

}
