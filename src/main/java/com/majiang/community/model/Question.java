package com.majiang.community.model;

import lombok.Data;

/**
 * @author yu
 * @date 2021/8/14 10:34
 */
@Data
public class Question {

    private Integer id;
    private String title;
    private String description;
    private String tag;
    private Long gmtCreate;
    private Long gmtModified;
    private Integer creator;
    private Integer viewCount;
    private Integer likeCount;
    private Integer commentCount;

}
