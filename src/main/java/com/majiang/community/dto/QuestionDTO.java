package com.majiang.community.dto;

import com.majiang.community.model.User;
import lombok.Data;

/**
 * @author yu
 * @date 2021/8/14 19:54
 */
@Data
public class QuestionDTO {

    private Integer id;
    private String title;
    private String description;
    private String tag;
    private Long gmtCreate;
    private Long gmtModified;
    private Integer creator;
    private Integer viewCount;
    private Integer likeCount;
    private Integer commentCount;

    private User user;
}
