package com.majiang.community.mapper;

import com.majiang.community.model.Question;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yu
 * @date 2021/8/14 10:31
 */
@Mapper
public interface QuestionMapper {
    @Insert("insert into question (title, description, gmt_create, gmt_modified, creator, tag) values (" +
            "#{title}, #{description}, #{gmtCreate}, #{gmtModified}, #{creator}, #{tag})")
    void create(Question question);

    @Select("select * from question limit ${page}, ${rows}")
    List<Question> list(@Param("page") Integer page, @Param("rows") Integer rows);

    @Select("select count(*) from question")
    Integer count();

    @Select("select count(*) from question where creator = #{userId}")
    Integer countByUserId(@Param("userId") Integer userId);

    @Select("select * from question where creator = #{userId} limit ${page}, ${rows}")
    List<Question> listByUserId(@Param("userId") Integer userId, @Param("page") Integer page, @Param("rows") Integer rows);
}
