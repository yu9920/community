package com.majiang.community.model;

import lombok.Data;

/**
 * @author yu
 * @date 2021/8/13 11:56
 */
@Data
public class User {

    private Integer id;
    private String name;
    private String accountId;
    private String token;
    private Long gmtCreate;
    private Long gmtModified;
    private String avatarUrl;

}
